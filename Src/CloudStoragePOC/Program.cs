﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TwentyTwenty.Storage;
using TwentyTwenty.Storage.Amazon;

namespace CloudStoragePOC
{
    class Program
    {
        static void Main(string[] args)
        {
            //MainAsync(args).GetAwaiter().GetResult();
            UploadScvToAmazonThenDownloadItAndCompareToOriginal(args).GetAwaiter().GetResult();
        }

        private static async Task UploadScvToAmazonThenDownloadItAndCompareToOriginal(string[] args)
        {
            IStorageProvider provider = new AmazonStorageProvider(new AmazonProviderOptions
            {
                Bucket = "cloud.storage.poc",
                PublicKey = "AKIAJTD5NHIGDIP2AILQ",
                SecretKey = "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",
                ServiceUrl = "https://s3-eu-west-1.amazonaws.com" // or empty
            });

            var containerName = "test";

            var blobName = Path.GetFileNameWithoutExtension("Data/Report.csv");
            
            var bytes = File.ReadAllBytes("Data/Report.csv");

            // ContainerName configurable?
            await provider.SaveBlobStreamAsync(containerName, blobName, new MemoryStream(bytes));

            var stream =  await provider.GetBlobStreamAsync(containerName, blobName);


            using (var fileStream = File.Create("Data/DownloadedReportUsingAmazonProvider.csv"))
            using (stream)
            {
                stream.CopyTo(fileStream);
            }

            var url = provider.GetBlobSasUrl(containerName, blobName, DateTimeOffset.UtcNow.AddDays(1), true); // uriExpiration should be configurable aswell?? TimeSpan??

            var webClient = new WebClient();
            byte[] downloadedBytes = webClient.DownloadData(url);

            File.WriteAllBytes("Data/DownloadedReport.csv", downloadedBytes);
        }

        static async Task MainAsync(string[] args)
        {
            IStorageProvider provider = new AmazonStorageProvider(new AmazonProviderOptions
            {
                Bucket = "cloud.storage.poc",
                PublicKey = "AKIAJTD5NHIGDIP2AILQ",
                SecretKey = "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",
                ServiceUrl = "https://s3-eu-west-1.amazonaws.com" // or empty
            });

            var containerName = "test"; // Should be configurable?
            var blobName = GenerateRandomName();
            var byteArray = GenerateRandomBlob();

            // Defualt blob properties can also be passed as an additional parameter
            await provider.SaveBlobStreamAsync(containerName, blobName, new MemoryStream(byteArray));

            var url = provider.GetBlobSasUrl(containerName, blobName, DateTimeOffset.UtcNow.AddDays(1), true);

            var webClient = new WebClient();
            byte[] downloadedBytes = webClient.DownloadData(url);

            var result = ArraysEqual(byteArray, downloadedBytes);

            Console.WriteLine(result);

            Console.ReadKey();
        }

        protected static byte[] GenerateRandomBlob(int length = 256)
        {
            Random rand = new Random();
            var buffer = new Byte[length];
            rand.NextBytes(buffer);
            return buffer;
        }

        protected static MemoryStream GenerateRandomBlobStream(int length = 256)
        {
            return new MemoryStream(GenerateRandomBlob(length));
        }

        protected static string GetRandomContainerName()
        {
            return "ContainerPrefix" + Guid.NewGuid().ToString("N");
        }

        protected static string GenerateRandomName()
        {
            return Guid.NewGuid().ToString("N");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="array1"></param>
        /// <param name="array2"></param>
        /// <param name="bytesToCompare"> 0 means compare entire arrays</param>
        /// <returns></returns>
        public static bool ArraysEqual(byte[] array1, byte[] array2, int bytesToCompare = 0)
        {
            if (array1.Length != array2.Length) return false;

            var length = (bytesToCompare == 0) ? array1.Length : bytesToCompare;
            var tailIdx = length - length % sizeof(Int64);

            //check in 8 byte chunks
            for (var i = 0; i < tailIdx; i += sizeof(Int64))
            {
                if (BitConverter.ToInt64(array1, i) != BitConverter.ToInt64(array2, i)) return false;
            }

            //check the remainder of the array, always shorter than 8 bytes
            for (var i = tailIdx; i < length; i++)
            {
                if (array1[i] != array2[i]) return false;
            }

            return true;
        }
    }
}
